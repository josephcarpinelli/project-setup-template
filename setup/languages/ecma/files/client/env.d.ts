interface ImportMetaEnv
{
  VITE_PORT?: string;
  VITE_API?: string;
  VITE_PROD?: string;
  VITE_AUTH_TOKEN?: string;
}

