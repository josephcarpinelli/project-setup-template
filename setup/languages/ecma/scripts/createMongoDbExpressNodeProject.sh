#!/bin/bash

# MERN stack backend project creation script.
# Joseph Carpinelli: 2024-01-06.

## ===================================================================== ##

## ===================================================================== ##


function createMongoDbExpressNodeProject()
{
  PROJECT_NAME="$1"
  PROJECT_DIR="$HOME/src/web/servers/$PROJECT_NAME"
  mkdir -p "$PROJECT_DIR"
  cd "$PROJECT_DIR"

  createMernServer $@
  gitSetup $@
  printFiles $@

  return 0
}


## ===================================================================== ##

function main()
{
  createMongoDbExpressNodeProject $@
  return 0
}


main $@
exit 0


## ===================================================================== ##

