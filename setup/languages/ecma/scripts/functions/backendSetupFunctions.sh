#!/bin/bash

# MERN backend stack project creation functions.
# Joseph Carpinelli: 2024-01-06.

## ===================================================================== ##

# Bash.

function console()
{
    printf '%s\n' "$@"
    return 0
}


## ===================================================================== ##


# Files.

function copyServerFiles()
{
  cp -av $HOME/src/general/project-setup-template/languages/ecma/files/server/. ./
  return 0
}

function printBackendFiles()
{
  console "1. LICENSE"
  console "2. README.md"
  console "3. CONTRIBUTING.md"
  console "4. scripts/"
  console "5. .env (get a generic version going)"
  console "6. tsconfig.ts"
  console "7. docs/"
  console "\t 1. wireframes"
  console "\t 2. kanban/stories"
  console "8. models/"
  console "9. views/"
  console "10. controllers/"
  console "11. routes/"
  console "12. config/"
  console ""
  console "Edit: "
  console "1. README.md"
  console "\t 1. Add Project Name and Description."

  return 0
}


## ===================================================================== ##

# Server/Backend.

function installMongoExpressNode()
{
  # Express and Auxilliary.
  npm install express 
  npm install -D @types/node @types/express
  # Auxilliary.
  npm install nodemon cors method-override
  npm install -D @types/cors
  # Basic View Rendering.
  npm install jsx-view-engine react react-dom
  npm install -D @types/react @types/react-dom @types/react-router-dom
  # MongoDB.
  npm install mongoose dotenv
  # Auth.
  npm install bcrypt jsonwebtoken
  npm install -D @types/jsonwebtoken

  return 0
}

function createMernServer()
{
  mkdir -p {models,views,controllers,routes,config,middleware,docs}
  touch server.ts
  npm init
  npm install
  installTypeScript $@
  installMongoExpressNode $@
  copyProjectBasics $@
  copyServerFiles $@

  return 0
}



## ===================================================================== ##

# Files.

function copyServerFiles()
{
  cp -av $HOME/src/general/project-setup-template/languages/ecma/files/server/ ./
  return 0
}

function copyMernFiles()
{
  cp -av $HOME/src/general/project-setup-template/languages/ecma/files/.gitignore ./
  copyProjectBasics $@
  copyClientFiles $@
  copyServerFiles $@
  return 0
}

function printFiles()
{
  console "1. LICENSE"
  console "2. README.md"
  console "3. CONTRIBUTING.md"
  console "4. scripts/"
  console "5. .env (get a generic version going)"
  console "6. tsconfig.ts"
  console "7. docs/"
  console "\t 1. wireframes"
  console "\t 2. kanban/stories"
  console "9. server/"
  console "\t 1. models/"
  console "\t 2. views/"
  console "\t 3. controllers/"
  console "\t 4. routes/"
  console "\t 5. config/"
  console ""
  console "Edit: "
  console "1. README.md"
  console "\t 1. Add Project Name and Description."
  console "2. ./client/"
  console "\t\t 4. main.{js,ts}"
  console "\t\t 5. App.{jsx,tsx}"

  return 0
}


## ===================================================================== ##

# Server/Backend.

function installMongoExpressNode()
{
  # Express and Auxilliary.
  npm install express 
  npm install -D @types/node @types/express
  # Auxilliary.
  npm install nodemon cors method-override
  # Basic View Rendering.
  npm install jsx-view-engine 
  # MongoDB.
  npm install mongoose dotenv
  # Auth.
  npm install bcrypt jsonwebtoken
  npm install -D @types/jsonwebtoken

  return 0
}


## ===================================================================== ##

# Main

function createMernServer()
{
  mkdir -p {server/{models,views,controllers,routes,config,middleware},docs}
  touch server/server.ts
  cd server/
  npm init
  npm install
  source ./installTypeScriptFunction.sh
  installTypeScript $@
  installMongoExpressNode $@
  cd ..
  # copyProjectBasics $@
  # copyServerFiles $@

  return 0
}


## ===================================================================== ##

