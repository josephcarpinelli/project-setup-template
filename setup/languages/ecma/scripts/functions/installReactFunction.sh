#!/bin/bash

# Install React function.
# Joseph Carpinelli: 2024-01-06.

## ===================================================================== ##

# Main

function installReact()
{
  npm install react react-dom react-router-dom
  npm install -D @types/react @types/react-dom @types/react-router-dom

  return
}


## ===================================================================== ##

